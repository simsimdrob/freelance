<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserRepository;
use function Sodium\add;

class FreelanceController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index()
    {
        return $this->render('freelance/home.html.twig');
    }

    /**
     * @Route("/cv", name="cv_page")
     */
    public function cv()
    {
        return $this->render('freelance/cv.html.twig');
    }

    /**
     * @Route("/form", name="form_page")
     */
    public function form(Request $request, EntityManagerInterface $manager, UserRepository $repo)
    {
        $user = new User();
        $form = $this->createFormBuilder($user)
                     ->add("firstName")
                     ->add("lastName")
                     ->add("email")
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid())
        {
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("home_page");
        }

        return $this->render('freelance/form.html.twig',
                      ['form' => $form->createView()]);
    }
}
